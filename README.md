# scenegen

An animated scene generator. May be useful for testing video motion detectors.

## Scene description

FIXME: describe how to create scenes.

## Development

Create and activate a Python 3.7 virtualenv:

```
$ python3.7 -m venv .venv && . .venv/bin/activate
```

Change to a branch:

```
$ git checkout -b my_branch
```

Install Poetry: https://python-poetry.org/docs/#installation

Install project deps and pre-commit hooks:

```
$ poetry install
$ pre-commit install
$ pre-commit run --all-files
```

Remember to activate your virtualenv whenever working on the repo, this is needed
because pylint and mypy pre-commit hooks use the "system" python for now (because reasons).
