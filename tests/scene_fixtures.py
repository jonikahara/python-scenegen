"""Scenes for testing"""
# pylint: disable=W0613

import pytest  # type: ignore


@pytest.fixture
def scene_fadeinout():  # type: ignore
    """A scene globally turning from black to white, then back to black"""

    def update_color(clock=0):  # type: ignore
        """The update function"""
        return (0, 0, 0)

    def _scene_fadeinout(width=512, height=512):  # type: ignore
        """The scene implementation"""

        return {
            "black_background": {"shape": "rectangle", "size": (width, height), "color": update_color,},
        }

    return _scene_fadeinout


@pytest.fixture
def scene_colorcircles():  # type: ignore
    """A scene with red, green, and blue circles circling the center, on a black background"""

    def _scene_colorcircles(width=512, height=512):  # type: ignore
        """The scene implementation"""

        circle_size = int(min(width, height) / 10)

        def update_position_red(clock=0):  # type: ignore
            """The red circle's position updater"""
            return (0, 0)

        def update_position_green(clock=0):  # type: ignore
            """The red circle's position updater"""
            return (0, 0)

        def update_position_blue(clock=0):  # type: ignore
            """The red circle's position updater"""
            return (0, 0)

        return {
            "black_background": {
                "shape": "rectangle",
                "center": (width / 2, height / 2),
                "size": (width, height),
                "color": (0, 0, 0),
            },
            "red_circle": {
                "enter": 0,
                "shape": "circle",
                "center": update_position_red,
                "size": circle_size,
                "color": (255, 0, 0),
            },
            "green_circle": {
                "enter": 180,
                "shape": "circle",
                "center": update_position_green,
                "size": circle_size,
                "color": (0, 255, 0),
            },
            "blue_circle": {
                "enter": 360,
                "shape": "circle",
                "center": update_position_blue,
                "size": circle_size,
                "color": (0, 0, 255),
            },
        }

    return _scene_colorcircles
